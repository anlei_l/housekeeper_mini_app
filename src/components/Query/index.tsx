import {useCallback, useEffect, useState} from "react";
import Request from '../../utils/Request'
import Taro from '@tarojs/taro'

const Query = ({ uri, initialQuery, children, pages = false }) => {
  const [ data, setData ] = useState();
  const [ error, setError ] = useState('');
  const [ loading, setLoading ] = useState(true)

  useEffect(() => {
    refresh(initialQuery)
  }, [JSON.stringify(initialQuery)])

  Taro.useDidShow(() => {
    refresh(initialQuery)
  })

  const refresh = useCallback(async (query) => {
    try {
      setLoading(true)
      const newQuery = {
        ...initialQuery,
        ...query
      }
      const newQuery2 = {}
      Object.keys(newQuery).filter(item => newQuery[item]).map(item => {
        newQuery2[item] = newQuery[item]
      })
      const result = await Request.get(uri, newQuery2)
      setData(v => {
        if (pages) {
          if (v instanceof Array) {
            return v.contact(result)
          }
          return result
        }
        return result
      })
      return result
    } catch (e) {
      console.log(e)
      setError(e)
    } finally {
      setLoading(false)
    }
  }, [])

  return children({refresh, data, loading, error })
}

export default Query
