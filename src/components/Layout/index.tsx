import {View} from "@tarojs/components";

import './index.scss'

const Layout = ({ children }) => {
  return (
    <View className="layout">
      { children }
    </View>
  )
}

export default Layout
