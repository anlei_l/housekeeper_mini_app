import {ScrollView, View} from "@tarojs/components";
import Taro from '@tarojs/taro'

import './index.scss'
import {useEffect} from "react";

const Container = ({ backgroundColor = '#fff', children, isTabBar = false }) => {

  return (
    <View className='container' style={{backgroundColor}}>
      { children }
    </View>
  )
}

export default Container
