import {useCallback, useEffect, useState} from "react";
import Request from '../../utils/Request'

const Mutation = ({ uri, children, method = 'post' }) => {
  const [ result, setResult ] = useState();
  const [ error, setError ] = useState('');
  const [ loading, setLoading ] = useState(false)

  const mutation = useCallback(async (body) => {
    try {
      setLoading(true)
      const res = await Request[method](uri, body)
      setResult(res)
      return res
    } catch (e) {
      console.log(e)
      setError(e)
    } finally {
      setLoading(false)
    }
  }, [])

  return children(mutation, {result, loading, error })
}

export default Mutation
