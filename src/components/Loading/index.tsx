import {Image, View} from "@tarojs/components";
import loadingGif from './loading.gif'
import loadingSvg from './loading.svg'

import './index.scss'

const Loading = () => {
  return (
    <View className="loading">
      <Image src={loadingSvg} mode="aspectFill" className="loading__img" />
    </View>
  )
}

export default Loading
