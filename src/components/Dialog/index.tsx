import {View} from "@tarojs/components";
import './index.scss'

const Dialog = ({ visable = false, title, children, onOk, onCancel }) => {
  if (!visable) {
    return null
  }
  return (
    <View className='dialog'>
      <View className='dialog__container'>
        <View className='dialog__wapper'>
          <View className='dialog__header'>
            {title}
          </View>
          <View className='dialog__content'>
            {children}
          </View>
          <View className='dialog__footer'>
            <View className='btn' onClick={onCancel}>取消</View>
            <View className='btn primary' onClick={onOk}>确定</View>
          </View>
        </View>
      </View>
    </View>
  )
}

export default Dialog
