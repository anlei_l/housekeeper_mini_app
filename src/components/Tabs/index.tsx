import {Image, Text, View} from "@tarojs/components";

import './index.scss'
import hookpng from '../../pages/home/hook.png'
import {useState} from "react";

const Tabs = ({ list = [], onChange }) => {
  const [ activetab, setActiveTab ] = useState(0)
  return (
    <View className="tabs">
      {
        list.map((item, index) => (
          <View className="tabs__item" onClick={() => {
            onChange(index)
            setActiveTab(index)
          }}>
            <Image src={activetab === index ? item.activeIcon : item.icon} className="tabs__item--icon" />
            <Text>{item.title}</Text>
          </View>
        ))
      }
    </View>
  )
}

export default Tabs
