import {Image, Text, View} from "@tarojs/components";
import './index.scss'
import hookpng from './hook.png'

const BottomTabs = ({ onChange }) => {
  return (
    <View className='bottomTabs'>
      <View className='bottomTabs__item' onClick={() => onChange('workbench')}>
        <Image src={hookpng} className='bottomTabs__item--img' />
        <Text className='bottomTabs__text'>工作台</Text>
      </View>
      <View className='bottomTabs__item' onClick={() => onChange('mine')}>
        <Image src={hookpng} className='bottomTabs__item--img' />
        <Text className='bottomTabs__text'>我的</Text>
      </View>
    </View>
  )
}

export default BottomTabs
