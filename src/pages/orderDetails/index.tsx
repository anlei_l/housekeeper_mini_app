import {Image, View, Text, ScrollView, Button} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {default as React, useCallback} from "react";
import moment from "moment";

import time from './time.png'
import Location from './Location.png'
import Document from './Document.png'
import coff from './coff.png'
import Mutation from "../../components/Mutation";

const orderStatus = [{
  title: '订单已生成',
  render: () => '订单已生成，请等待工程师联系'
}, {
  title: '等待工程师接单',
  render: (data) => `请等待工程师接单`
}, {
  title: '工程师已经接单',
  render: (data) => `工程师已接单，请等待工程师联系`
}, {
  title: '等待工程师上门',
  render: (data) => `工程师上门时间：${data?.upDoorTime}`
}, {
  title: '订单检测完成',
  render: () => '订单已经检测完成，请及时支付订金'
}, {
  title: '订单等待服务',
  render: () => '订金支付完成，请等待服务'
}, {
  title: '等待支付尾款',
  render: () => '订单已完成，请及时支付尾款'
}, {
  title: '订单已完成',
  render: () => '订单已完成'
}]

const OrderDetails = () => {
  const router = Taro.useRouter()

  const handleCancel = useCallback((mutation, id) => {
    console.log(123123)
  }, [])

  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri={`/order/${router.params.id}`}>
        {
          ({ data, loading, error, refresh }) => {
            return (
              <>
                <ScrollView scrollY style={{flexGrow: 1, flexShrink: 1, height: '50%'}}>
                  <View className='details'>
                    <View className='info process'>
                      <View className='process__tips'>
                        <Image src={coff} className='icon' />
                        <Text>如发现工程师中途无故加价，请拒绝付款！</Text>
                      </View>
                      <View className='process__content'>
                        {
                          data?.logicStatus !== 0 ? orderStatus.map((item, index) => {

                            return (
                              <View className={`node ${data?.logicStatus > index ? 'complete' : ''}`}>
                                <View className='node__name'>
                                  <Text>{item.title}</Text>
                                  {/*<Text className='time'>{moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}</Text>*/}
                                </View>
                                <View className='node__extra'>
                                  { item.render && item.render(data) }
                                </View>
                              </View>
                            )
                          }) : (
                            <View className={`node complete`}>
                              <View className='node__name'>
                                <Text>订单已取消</Text>
                              </View>
                              <View className='node__extra'>
                                用户已取消订单
                              </View>
                            </View>
                          )
                        }
                      </View>
                    </View>
                    <View className='info repair'>
                      <View className='info__title'>预约信息</View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={time} className='icon__img' />
                        </View>
                        <View className='text'>{moment(data?.createAt).format('YYYY-MM-DD hh:mm:ss')}</View>
                      </View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={Location} className='icon' />
                        </View>
                        <View className='text'>
                          <Text>
                            {data?.createUser?.nickName} <Text className='phone'>{data?.createUser?.phoneNumber}</Text>
                          </Text>
                          <Text>
                            {data?.address?.province}
                            {data?.address?.city}
                            {data?.address?.area}
                            {data?.address?.address}
                          </Text>
                        </View>
                      </View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={Document} className='icon__img' />
                        </View>
                        <View className='text'>{ data?.project?.name }</View>
                      </View>
                    </View>

                    {
                      !!data?.quotationRecords.length && (
                        <View className='info quotation'>
                          <View className='info__title'>报价单信息</View>
                          {
                            data?.quotationRecords.map(item => {
                              return (
                                <View className='repair__item'>
                                  <View className='icon'>
                                    <Image mode="aspectFill" src={item?.quotation?.cover} className='icon' />
                                  </View>
                                  <View className='text'>
                                    <Text>
                                      { item?.quotation?.name }
                                    </Text>
                                    <Text>
                                      X {item?.num}
                                    </Text>
                                  </View>
                                </View>
                              )
                            })
                          }
                        </View>
                      )
                    }

                    {
                      !!data?.images && (
                        <View className='info images'>
                          <View className='info__title'>图片</View>
                          <View className="list">
                              {
                                data?.images.split(',').map(item => (
                                  <View className='order__item' onClick={() => {
                                    Taro.previewImage({
                                      urls: data?.images.split(',')
                                    })
                                  }}>
                                    <Image mode="aspectFill" src={item} />
                                  </View>
                                ))
                              }
                          </View>
                        </View>
                      )
                    }

                    {
                      !!data?.completePictures && (
                        <View className='info images'>
                          <View className='info__title'>维修图片</View>
                          <View className="list">
                            {
                              data?.completePictures.split(',').map(item => (
                                <View className='order__item' onClick={() => {
                                  Taro.previewImage({
                                    urls: data?.completePictures.split(',')
                                  })
                                }}>
                                  <Image mode="aspectFill" src={item} />
                                </View>
                              ))
                            }
                          </View>
                        </View>
                      )
                    }

                    <View className='info order'>
                      <View className='info__title'>订单信息</View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订单号
                        </Text>
                        <View className='text'>
                          <Text>{data?.orderNo}</Text>
                          <View className='copy' onClick={() => {
                            Taro.setClipboardData({
                              data: data?.orderNo
                            })
                          }}
                          >复制</View>
                        </View>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          下单时间
                        </Text>
                        <Text>
                          {moment(data?.createAt).format('YYYY-MM-DD HH:mm:ss')}
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订单金额
                        </Text>
                        <Text>
                          {data?.price}元
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订金
                        </Text>
                        <Text>
                          {data?.deposit}元
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          备注
                        </Text>
                        <Text>
                          {data?.remark}
                        </Text>
                      </View>
                    </View>
                  </View>
                </ScrollView>
                <View className='actions'>
                  {
                    (data?.logicStatus !== 0 && data?.logicStatus <= 5) && (
                      <Mutation uri='/order/cancel'>
                        {
                          (mutation) => (
                            <View className='btn' onClick={() => {

                              Taro.showModal({
                                title: '警告',
                                content: '您确定要取消这个订单么？',
                                confirmText: '确定',
                                cancelText: '取消',
                                success: (result) => {
                                  if (result.confirm) {
                                    mutation({ id: data?.id })
                                      .then(res => {
                                        Taro.showToast({
                                          title: '取消成功',
                                          icon: null
                                        })
                                        setTimeout(() => {
                                          Taro.navigateBack()
                                        }, 2000)
                                      })
                                  } else if (result.cancel) {

                                  }
                                }
                              })
                            }}
                            >取消订单</View>
                          )
                        }
                      </Mutation>
                    )
                  }
                  {
                    data?.logicStatus === 5 && (
                      <Mutation uri='/pay/depositInfo'>
                        {
                          (mutation) => (
                            <View
                              className='btn'
                              onClick={
                                (e) => {
                                  e.stopPropagation()

                                  mutation(data)
                                    .then(res => {
                                      console.log(res)

                                      Taro.requestPayment({
                                        ...res,
                                        success: function (res2) {
                                          Taro.navigateTo({
                                            url: `/pages/payResult/index?orderNo=${data.orderNo}`
                                          })
                                        },
                                        fail: function (res2) {
                                          // Taro.showToast({
                                          //   icon: 'none',
                                          // })
                                        }
                                      })
                                    })
                                  return
                                }
                              }
                            >
                              支付订金
                            </View>
                          )
                        }
                      </Mutation>
                    )
                  }
                  {
                    data?.logicStatus === 7 && (
                      <Mutation uri='/pay/info'>
                        {
                          (mutation) => (
                            <View
                              className='btn'
                              onClick={
                                (e) => {
                                  e.stopPropagation()

                                  mutation(data)
                                    .then(res => {
                                      console.log(res)

                                      Taro.requestPayment({
                                        ...res,
                                        success: function (res2) {
                                          Taro.navigateTo({
                                            url: `/pages/payResult/index?orderNo=${data.orderNo}`
                                          })
                                        }
                                      })
                                    })

                                  return
                                }
                              }
                            >
                              支付费用
                            </View>
                          )
                        }
                      </Mutation>
                    )
                  }
                  <Button openType='contact' className='btn primary'>联系客服</Button>
                </View>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default OrderDetails;
