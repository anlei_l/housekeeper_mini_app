import { useCallback, useEffect } from "react";
import { View, Text, Button, Image } from "@tarojs/components";
import './index.scss'
import arrowRight from '../workbench/icon/arrowRight.png'
import Query from "../../components/Query";
import Mutation from "../../components/Mutation";

import Notification from './Notification.png'
import Paper from './Paper.png'
import phone from './phone.png'
import photoPng from './photo.png'
import Taro from '@tarojs/taro'

const EngineerMine = () => {
  const receiveIngOrder = useCallback((mutation, refresh, isReceiving) => {
    mutation()
      .then(res => {
        if (!isReceiving) {
          Taro.requestSubscribeMessage({
            tmplIds: ['jeBT8tnU7xDPfhJreek5xLskgGcvRcInQz1a2k0Z6WM'],
            success: (res) => {
              console.log(res)
            }
          })
        }
        refresh()
      })
  }, [])

 return (
   <Query uri='/user/profile'>
     {
       ({ data, loading, error, refresh }) => {
         return (
           <>
             <View className='user'>
               <View className='user__info'>
                 <Text className='user__info--name'>{data?.nickName}</Text>
                 <Text className='user__info--role'>{ data?.project?.name }</Text>
               </View>
               <View className='user__photo'>
                 <Image src={photoPng} className='user__photo--img' />
               </View>
             </View>
             <View className='account'>
               {
                 data?.role === 2 && (
                   <Mutation uri='/user/startOrStopReceiving' method='put'>
                     {
                       (mutation) => (
                         <View className='account__receiving' onClick={() => receiveIngOrder(mutation, refresh, data?.isReceiving)}>
                           <Image src={Notification} className='account__receiving--img' />
                           <Text className='account__receiving--text'>
                             {
                               data?.isReceiving ? '停止接单' : '开始接单'
                             }
                           </Text>
                         </View>
                       )
                     }
                   </Mutation>
                 )
               }
               <View className='account__info'>
                 <Text className='account__info--title'>累计收益</Text>
                 <Text className='account__info--content'>￥{ Number(data?.withdrawalAmount).toFixed(2) }</Text>
                 <Text className='account__info--title'>可提现金额</Text>
                 <Text className='account__info--content'>￥{Number(data?.withdrawableCash).toFixed(2)}</Text>
                 <View className='account__info--actions'>
                   <Button className='btn account-btn' onClick={() => {
                     Taro.navigateTo({
                       url: '/pages/shareRecord/index'
                     })
                   }}
                   >账单明细</Button>
                   <Mutation uri='/withdrawalRecord'>
                     {
                       (mutation) => {
                         return (
                           <Button className='btn account-btn' onClick={() => {
                             if (data?.withdrawableCash > 0) {
                               Taro.showModal({
                                 title: '提示',
                                 content: `您有${data?.withdrawableCash}元可以提现，确认提现么？`,
                                 success: function (res) {
                                   if (res.confirm) {
                                     mutation()
                                       .then(res => {
                                         Taro.showToast({
                                           title: '提现申请已提交，请等待管理员审核',
                                           icon: 'none'
                                         })
                                       })
                                   } else if (res.cancel) {
                                     console.log('用户点击取消')
                                   }
                                 }
                               })
                               return
                             }

                             Taro.showToast({
                               title: '对不起，您暂无可提现金额',
                               icon: 'none'
                             })
                           }}
                           >提现</Button>
                         )
                       }
                     }
                   </Mutation>
                 </View>
               </View>
             </View>
             <View className='tool'>
               <View className='tool__title'>
                 <View className='tool__title--text'>管家工具</View>
               </View>
               <View className='tool__list'>
                 {
                   data?.role === 3 && (
                     <Button className='tool__list--item' onClick={() => {
                       Taro.navigateTo({
                         url: '/pages/singleCode/index'
                       })
                     }}>
                       <View className='icon'>
                         <Image src={Paper} className='icon__img' />
                       </View>
                       <Text className='title'>查看下单码</Text>
                       <Image src={arrowRight} className='arrowRight' />
                     </Button>
                   )
                 }
                 <Button openType="contact" className='tool__list--item'>
                   <View className='icon blue'>
                     <Image src={phone} className='icon__img' />
                   </View>
                   <Text className='title'>联系客服</Text>
                   <Image src={arrowRight} className='arrowRight' />
                 </Button>
               </View>
             </View>
           </>
         )
       }
     }
   </Query>
 )
};

export default EngineerMine;
