import * as React from "react";
import {Image, View, Text, Button, Input} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Stepper from "./components/Stepper";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import Loading from "../../components/Loading";

const ShareRecord = () => {
  const role = Taro.getStorageSync('userInfo').role
  const initialQuery = role === 2 ? { logicStatus: 8 } : {}
  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <Query uri={role === 2 ? `/order/engineer` : `/shareRecord`} initialQuery={initialQuery}>
        {
          ({ data, loading }) => {
            if (loading) {
              return <Loading />
            }
            return (
              <View className="quotation">
                {
                  data.map(item => {
                    const newData = item?.order || item
                    return (
                      <View className="record">
                        <View className="record__title">订单号: {newData?.orderNo }</View>
                        <View className="record__content">
                          <View className="time">{ newData?.createAt }</View>
                          <View className="price">
                            收取费用
                            <Text>
                              ￥{ item?.price }
                            </Text>
                          </View>
                        </View>
                      </View>
                    )
                  })
                }
              </View>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default ShareRecord;
