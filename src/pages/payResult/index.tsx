import {Image, View, Text, ScrollView, Input} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import successPng from './success.png'
import starsPng from './stars.png'
import starsActivePng from './starsActive.png'
import {useState} from "react";
import Mutation from "../../components/Mutation";



const OrderDetails = () => {
  const router = Taro.useRouter()
  const [ activeStar, setActiveStar ] = useState(-1)
  const [ opinion, setOpinion ] = useState('')

  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri='/pay/order' initialQuery={{ orderNo: router.params.orderNo }}>
        {
          ({ data, loading, error, refresh }) => {
            console.log(data)
            return (
              <View className='content'>
                <View className='result'>
                  <Image src={successPng} />
                  <View className='price'>{ data?.logicStatus === 8 ? Number(data?.price - data?.deposit).toFixed(2) : data?.deposit }</View>
                  <View className='message'>{ data?.message }</View>
                  {/*<View className='order'>*/}
                  {/*  <Image src={data?.images.split(',')[0]} className='preview' />*/}
                  {/*  <View className='order__content'>*/}
                  {/*    <View className='title'>{ data?.project?.name }</View>*/}
                  {/*    <View className='time'>{ data?.updateAt }</View>*/}
                  {/*  </View>*/}
                  {/*</View>*/}
                  <View className='details' onClick={() => {
                    Taro.navigateTo({
                      url: `/pages/orderDetails/index?id=${data.id}`
                    })
                  }}
                  >查看订单</View>
                </View>
                {
                  data?.logicStatus === 8 && (
                    <View className='evaluate'>
                      <View className='evaluate__title'>
                        您对本次维修满意吗？
                      </View>
                      <View className='order'>
                        <Image mode="aspectFill" src={data?.images ? data?.images.split(',')[0] : data?.project?.thumbnail} className='preview' />
                        <View className='order__content'>
                          <View className='title'>{ data?.project?.name }</View>
                          <View className='time'>{ data?.updateAt }</View>
                          <View className='evaluate__content'>
                            <View className='evaluate__content--title'>评价</View>
                            <View className='evaluate__content--stars'>
                              {
                                ['', '', '', '', ''].map((item, index) => {
                                  if (activeStar >= index) {
                                    return (
                                      <Image src={starsActivePng} onClick={() => setActiveStar(index)} />
                                    )
                                  }
                                  return (
                                    <Image src={starsPng} onClick={() => setActiveStar(index)} />
                                  )
                                })
                              }
                            </View>
                            <View className='evaluate__content--title'>备注</View>
                            <View className='evaluate__content--stars'>
                              <Input placeholder="请输入您的意见" onInput={(evt) => {
                                setOpinion(evt.detail.value)
                              }} />
                            </View>
                          </View>
                        </View>
                      </View>
                      <Mutation uri="/order/score">
                        {
                          (mutation) => (
                            <View className='details' onClick={() => {
                              mutation({
                                id: data?.id,
                                score: activeStar,
                                opinion
                              })
                                .then(res => {
                                  Taro.showToast({
                                    title: '评价成功',
                                    icon: 'none'
                                  })

                                  setTimeout(() => {
                                    Taro.navigateTo({
                                      url: `/pages/orderDetails/index?id=${data.id}`
                                    })
                                  }, 2000)
                                })
                            }}
                            >提交</View>
                          )
                        }
                      </Mutation>
                    </View>
                  )
                }
              </View>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default OrderDetails;
