import { useCallback, useEffect } from "react";
import { View, Text, Button, Image } from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'

import paper from './paper.png'
import Download from './Download.png'
import officeBuilding from './office-building.png'
import briefcase from './briefcase.png'
import clipboard from './clipboard.png'
import arrowRight from '../workbench/icon/arrowRight.png'
import location from './Location.png'
import phone from './phone.png'
import ruzhu from './ruzhu.png'
import photoPng from './photo.png'

const Mine = () => {
 return (
   <View className="wapper">
     <Query uri="/user/profile">
       {
         ({ data, loading, error }) => {
           return (
             <>
               <View className='user'>
                 <View className='user__info'>
                   <Image src={photoPng} mode="aspectFill" className='user__info--photo' />
                   <View className='user__info--name'>
                     <Text>{data?.nickName === '微信用户' ? data?.phoneNumber : data?.nickName}</Text>
                     {/*<Text className='sub'>DJFHHKSK</Text>*/}
                   </View>
                   <View className='user__info--actions'>
                     {/*<View className='btn primary'>优惠券</View>*/}
                     <View className='btn' onClick={() => {
                       Taro.navigateTo({
                         url: '/pages/warrantyCard/index'
                       })
                     }}>保修卡</View>
                   </View>
                 </View>
                 <View className='user__order'>
                   <View className='user__order--item' onClick={() => {
                     Taro.navigateTo({
                       url: `/pages/order/index`
                     })
                   }}>
                     <Image src={clipboard} mode="aspectFill" className='icon' />
                     <Text>全部订单</Text>
                   </View>
                   <View className='user__order--item' onClick={() => {
                     Taro.navigateTo({
                       url: `/pages/order/index?type=1`
                     })
                   }}>
                     <Image src={clipboard} mode="aspectFill" className='icon' />
                     <Text>新订单</Text>
                   </View>
                   <View className='user__order--item' onClick={() => {
                     Taro.navigateTo({
                       url: `/pages/order/index?type=2`
                     })
                   }}>
                     <Image src={officeBuilding} mode="aspectFill" className='icon' />
                     <Text>进行中</Text>
                   </View>
                   <View className='user__order--item' onClick={() => {
                     Taro.navigateTo({
                       url: `/pages/order/index?type=3`
                     })
                   }}>
                     <Image src={briefcase} mode="aspectFill" className='icon' />
                     <Text>已完成</Text>
                   </View>
                 </View>
               </View>
               <View className="tool__container">
                 <View className='tool'>
                   <View className='tool__title'>
                     服务与工具
                   </View>
                   <View className='tool__list'>
                     <View className='tool__list--item' onClick={() => {
                       Taro.navigateTo({
                         url: '/pages/address/index'
                       })
                     }}>
                       <View className="icon">
                         <Image src={location} mode="aspectFill" className='icon__img' />
                       </View>
                       <Text className="text">地址管理</Text>
                     </View>
                     <Button openType="contact" className='tool__list--item'>
                       <View className="icon blue">
                         <Image src={paper} mode="aspectFill" className='icon__img' />
                       </View>
                       <Text className="text">联系客服</Text>
                     </Button>
                     {
                       !data?.applyRecord && (
                         <View className='tool__list--item' onClick={() => {
                           Taro.navigateTo({
                             url: '/pages/settled/index'
                           })
                         }}>
                           <View className="icon green">
                             <Image src={Download} mode="aspectFill" className='icon__img' />
                           </View>
                           <Text className="text">我要入驻</Text>
                         </View>
                       )
                     }
                   </View>
                 </View>
               </View>
             </>
           )
         }
       }
     </Query>
   </View>
 )
};

export default Mine;
