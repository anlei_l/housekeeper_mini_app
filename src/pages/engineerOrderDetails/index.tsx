import {Image, View, Text, ScrollView, Button, Picker, Input} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {default as React, useCallback, useState} from "react";
import moment from "moment";

import time from './time.png'
import Location from './Location.png'
import Document from './Document.png'
import coff from './coff.png'
import Mutation from "../../components/Mutation";
import Dialog from "../../components/Dialog";

const orderStatus = [{
  title: '订单已生成',
  render: () => '订单已生成，请等待工程师接单'
}, {
  title: '等待工程师接单',
  render: (data) => `请等待工程师接单`
}, {
  title: '工程师已经接单',
  render: (data) => `工程师已接单，请等待工程师联系`
}, {
  title: '等待工程师上门',
  render: (data) => `工程师上门时间：${data?.upDoorTime}`
}, {
  title: '订单检测完成',
  render: () => '订单已经检测完成，请及时支付订金'
}, {
  title: '订单等待服务',
  render: () => '订金支付完成，请等待服务'
}, {
  title: '等待支付尾款',
  render: () => '订单已完成，请及时支付尾款'
}, {
  title: '订单已完成',
  render: () => '订单已完成'
}]

const OrderDetails = () => {
  const router = Taro.useRouter()
  const [ isVisable, setIsVisable ] = useState(false)
  const [ isDepositVisable, setIsDepositVisable ] = useState(false)
  const [ formData, setFormData ] = useState({})
  const [ depositFormData, setDepositFormData ] = useState({})

  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri={`/order/${router.params.id}`}>
        {
          ({ data, loading, error, refresh }) => {
            return (
              <>
                <ScrollView scrollY style={{flexGrow: 1, flexShrink: 1, height: '50%'}}>
                  <View className='details'>
                    <View className='info process'>
                      <View className='process__tips'>
                        <Image src={coff} className='icon' />
                        <Text>如发现工程师中途无故加价，请拒绝付款！</Text>
                      </View>
                      <View className='process__content'>
                        {
                          data?.logicStatus !== 0 ? orderStatus.map((item, index) => {

                            return (
                              <View className={`node ${data?.logicStatus > index ? 'complete' : ''}`}>
                                <View className='node__name'>
                                  <Text>{item.title}</Text>
                                  {/*<Text className='time'>{moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}</Text>*/}
                                </View>
                                <View className='node__extra'>
                                  { item.render && item.render(data) }
                                </View>
                              </View>
                            )
                          }) : (
                            <View className={`node complete`}>
                              <View className='node__name'>
                                <Text>订单已取消</Text>
                              </View>
                              <View className='node__extra'>
                                用户已取消订单
                              </View>
                            </View>
                          )
                        }
                      </View>
                    </View>
                    <View className='info repair'>
                      <View className='info__title'>预约信息</View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={time} className='icon__img' />
                        </View>
                        <View className='text'>{moment(data?.createAt).format('YYYY-MM-DD hh:mm:ss')}</View>
                      </View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={Location} className='icon' />
                        </View>
                        <View className='text'>
                          <Text>
                            {data?.createUser?.nickName} <Text className='phone'>{data?.createUser?.phoneNumber}</Text>
                          </Text>
                          <Text>
                            {data?.address?.province}
                            {data?.address?.city}
                            {data?.address?.area}
                            {data?.address?.address}
                          </Text>
                        </View>
                      </View>
                      <View className='repair__item'>
                        <View className='icon'>
                          <Image src={Document} className='icon__img' />
                        </View>
                        <View className='text'>{ data?.project?.name }</View>
                      </View>
                    </View>

                    {
                      !!data?.quotationRecords.length && (
                        <View className='info quotation'>
                          <View className='info__title'>报价单信息</View>
                          {
                            data?.quotationRecords.map(item => {
                              return (
                                <View className='repair__item'>
                                  <View className='icon'>
                                    <Image mode="aspectFill" src={item?.quotation?.cover} className='icon' />
                                  </View>
                                  <View className='text'>
                                    <Text>
                                      { item?.quotation?.name }
                                    </Text>
                                    <Text>
                                      X {item?.num}
                                    </Text>
                                  </View>
                                </View>
                              )
                            })
                          }
                        </View>
                      )
                    }

                    {
                      !!data?.images && (
                        <View className='info images'>
                          <View className='info__title'>问题图片</View>
                          <View className="list">
                            {
                              data?.images.split(',').map(item => (
                                <View className='order__item' onClick={() => {
                                  Taro.previewImage({
                                    urls: data?.images.split(',')
                                  })
                                }}>
                                  <Image mode="aspectFill" src={item} />
                                </View>
                              ))
                            }
                          </View>
                        </View>
                      )
                    }

                    {
                      !!data?.completePictures && (
                        <View className='info images'>
                          <View className='info__title'>维修图片</View>
                          <View className="list">
                            {
                              data?.completePictures.split(',').map(item => (
                                <View className='order__item' onClick={() => {
                                  Taro.previewImage({
                                    urls: data?.completePictures.split(',')
                                  })
                                }}>
                                  <Image mode="aspectFill" src={item} />
                                </View>
                              ))
                            }
                          </View>
                        </View>
                      )
                    }

                    <View className='info order'>
                      <View className='info__title'>订单信息</View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订单号
                        </Text>
                        <View className='text'>
                          <Text>{data?.orderNo}</Text>
                          <View className='copy' onClick={() => {
                            Taro.setClipboardData({
                              data: data?.orderNo
                            })
                          }}
                          >复制</View>
                        </View>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          下单时间
                        </Text>
                        <Text>
                          {moment(data?.createAt).format('YYYY-MM-DD HH:mm:ss')}
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订单金额
                        </Text>
                        <Text>
                          {data?.price}元
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          订金
                        </Text>
                        <Text>
                          {data?.deposit}元
                        </Text>
                      </View>
                      <View className='order__item'>
                        <Text className='order__item--title'>
                          备注
                        </Text>
                        <Text>
                          {data?.remark}
                        </Text>
                      </View>
                    </View>
                  </View>
                </ScrollView>
                <View className='actions'>
                  {
                    data?.logicStatus <= 4 && data?.upDoorTime && (
                      <Button className='btn'>
                        { data?.upDoorTime ? data?.upDoorTime : '设置上门服务时间' }
                      </Button>
                    )
                  }
                  {
                    data?.logicStatus === 3 && (
                      <Button
                        className='btn primary'
                        onClick={(e) => {
                          e.stopPropagation()
                          Taro.showModal({
                            title: '警告',
                            content: '请先联系客户约定上门时间，若已经联系过客户，请设置上门时间',
                            confirmText: '设置时间',
                            cancelText: '联系客户',
                            success: (result) => {
                              if (result.confirm) {
                                setFormData((v) => ({
                                  ...v,
                                  id: data?.id
                                }))
                                setIsVisable(true)
                              } else if (result.cancel) {
                                Taro.makePhoneCall({
                                  phoneNumber: data?.createUser.phoneNumber
                                })
                              }
                            }
                          })

                        }}
                      >
                        预约上门
                      </Button>
                    )
                  }
                  {
                    (data?.logicStatus === 4) && (
                      <Button
                        className='btn primary'
                        onClick={(e) => {
                          e.stopPropagation()
                          Taro.navigateTo({
                            url: `/pages/quotation/index?id=${data?.id}`
                          })
                          refresh()
                        }}
                      >
                        收取订金
                      </Button>
                    )
                  }
                  {
                    (data?.logicStatus === 6) && (
                      <Mutation uri="/order/complete">
                        {
                          (mutation) => (
                            <Button
                              className='btn'
                              onClick={(e) => {
                                e.stopPropagation()
                                Taro.navigateTo({
                                  url: `/pages/quotation/index?id=${data.id}`
                                })
                              }}
                            >
                              修改报价单
                            </Button>
                          )
                        }
                      </Mutation>
                    )
                  }
                  {
                    (data?.logicStatus === 6) && (
                      <Mutation uri="/order/complete">
                        {
                          (mutation) => (
                            <Button
                              className='btn primary'
                              onClick={(e) => {
                                e.stopPropagation()
                                Taro.navigateTo({
                                  url: `/pages/completeOrder/index?id=${data.id}`
                                })
                              }}
                            >
                              订单完成
                            </Button>
                          )
                        }
                      </Mutation>
                    )
                  }
                  <Button openType='contact' className='btn primary'>联系客服</Button>
                </View>
                <Mutation uri='/order/makeAnAppointment'>
                  {
                    (mutation, { data, loading }) => {
                      return (
                        <Dialog
                          visable={isVisable}
                          title='请选择上门时间'
                          onCancel={() => setIsVisable(false)}
                          onOk={() => {
                            mutation({
                              ...formData,
                              upDoorTime: `${formData.date} ${formData.time}`
                            })
                              .then(res => {
                                refresh()
                              })
                            setIsVisable(false)
                          }}
                        >
                          <View className='form'>
                            <View className='form__item'>
                              <Picker
                                onChange={(e) => {
                                  setFormData((v) => {
                                    return {
                                      ...v,
                                      date: e.detail.value
                                    };
                                  })
                                }}
                                mode='date'
                              >
                                <Input className='input' value={formData.date} placeholder='年/月/日' disabled />
                              </Picker>
                            </View>
                            <View className='form__item'>
                              <Picker
                                onChange={(e) => {
                                  setFormData((v) => {
                                    return {
                                      ...v,
                                      time: e.detail.value
                                    };
                                  })
                                }}
                                mode='time'
                              >
                                <Input className='input' value={formData.time} placeholder='时/分' disabled />
                              </Picker>
                            </View>
                          </View>
                        </Dialog>
                      )
                    }
                  }
                </Mutation>

                <Mutation uri='/order/deposit'>
                  {
                    (mutation, { data, loading }) => {
                      return (
                        <Dialog
                          visable={isDepositVisable}
                          title='请选择您此次需要收取的定金'
                          onCancel={() => setIsVisable(false)}
                          onOk={() => {
                            console.log(depositFormData)
                            mutation({
                              ...depositFormData,
                            })
                              .then(res => {
                                refresh()
                                setIsDepositVisable(false)
                              })
                          }}
                        >
                          <View className='form'>
                            <View className='form__item'>
                              <View className='input' onClick={() => setDepositFormData({ ...depositFormData, deposit: 0 })}>
                                无需订金
                              </View>
                            </View>
                            <View className='form__item'>
                              <View className='input' onClick={() => setDepositFormData({ ...depositFormData, deposit: 50 })}>
                                ￥50
                              </View>
                            </View>
                            <View className='form__item'>
                              <Input
                                className='input'
                                placeholder="自定义价格"
                                onInput={(e) => {
                                  setDepositFormData({ ...depositFormData, deposit: e.detail.value })
                                }}
                              />
                            </View>
                          </View>
                        </Dialog>
                      )
                    }
                  }
                </Mutation>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default OrderDetails;
