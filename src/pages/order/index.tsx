import {Image, View, Text, Input, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Search from './Search.png'
import {useState} from "react";
import OrderItem from "./components/OrderItem";
import Query from "../../components/Query";
import Mutation from "../../components/Mutation";
import Loading from "../../components/Loading";
import Taro from '@tarojs/taro'

const tabs = [{
  text: '新订单',
  type: 1
}, {
  text: '进行中',
  type: 2
}, {
  text: '已完成',
  type: 3
}]

const Order = () => {
  const router = Taro.useRouter()
  const [ activeTab, setActiveTab ] = useState(router.params.type)
  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri="/order" initialQuery={{status: activeTab}}>
        {
          ({ data, loading, error, refresh }) => {
            return (
              <>
                {/*<View className='search'>*/}
                {/*  <Image src={Search} className='search__icon' />*/}
                {/*  <Input placeholder='请输入关键词搜索' className='search__input' placeholderClass='search__input--plc' />*/}
                {/*</View>*/}
                <View className='tabs'>
                  <View className={`tabs__item ${!activeTab ? 'actived': ''}`} onClick={() => {
                    refresh()
                    setActiveTab(undefined)
                  }}
                  >
                    <Text className='tabs__item--text'>全部</Text>
                  </View>
                  {
                    tabs.map((item, index) => {
                      return (
                        <View className={`tabs__item ${Number(activeTab) === item.type ? 'actived': ''}`} key={index} onClick={() => {
                          // refresh(item.type)
                          setActiveTab(item.type)
                        }}
                        >
                          <Text className='tabs__item--text'>{item.text}</Text>
                        </View>
                      )
                    })
                  }
                </View>
                {
                  loading ? (
                    <Loading />
                  ) : (
                    <ScrollView scrollY style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
                      <View className='orderList'>
                        {/*<DefaultPage />*/}
                        {
                          data.map(item => (
                            <Mutation uri="/pay">
                              {
                                (pay) => (
                                  <OrderItem
                                    record={item}
                                    onCancelOrder={() => refresh()}
                                    onPayDeposit={pay}
                                  />
                                )
                              }
                            </Mutation>
                          ))
                        }
                      </View>
                    </ScrollView>
                  )
                }
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default Order;
