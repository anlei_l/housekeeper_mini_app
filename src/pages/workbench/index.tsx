import {Button, Image, View, Swiper, SwiperItem, Text, ScrollView, Picker, Input} from "@tarojs/components";
import Taro from '@tarojs/taro'
import Request from '../../utils/Request'

import nexwOrder from './icon/newOrder.png'
import toBeCheck from './icon/toBeCheck.png'
import ToBeServed from './icon/toBeServed.png'
import completed from './icon/completed.png'
import cancel from './icon/cancel.png'
import arrowRight from './icon/arrowRight.png'
import paper from './icon/paper.png'
import home from '../home/Home.png'
import activeHome from '../home/activeHome.png'
import profile from '../home/profile.png'
import activeprofile from '../home/activeProfile.png'
import phone from '../order/phone.png'


import './index.scss'
import Container from "../../components/Container";
import Tabs from "../../components/Tabs";
import {default as React, useCallback, useEffect, useState} from "react";
import EngineerMine from "../engineerMine";
import Query from "../../components/Query";
import Dialog from "../../components/Dialog";
import Mutation from "../../components/Mutation";
import Loading from "../../components/Loading";

const tabs = [{
  text: '全部',
  icon: nexwOrder,
  status: -1,
}, {
  text: '新订单',
  icon: toBeCheck,
  status: 1,
}, {
  text: '进行中',
  icon: ToBeServed,
  status: 2,
}, {
  text: '已完成',
  icon: completed,
  status: 3,
}, {
  text: '已取消',
  icon: cancel,
  status: 0,
}]

const tabList = [{
  icon: home,
  activeIcon: activeHome,
  title: '首页',
  url: '/pages/home/index'
}, {
  icon: profile,
  activeIcon: activeprofile,
  title: '我的',
  url: '/pages/mine/index'
}]

const Workbench = () => {
  const role = Taro.getStorageSync('userInfo').role
  const [ activeTab, setActiveTab ] = useState(role === 3 ? 1 : 0)
  const [ status, setStatus ] = useState(undefined)
  const [ isVisable, setIsVisable ] = useState(false)
  const [ isDepositVisable, setIsDepositVisable ] = useState(false)
  const [ formData, setFormData ] = useState({})
  const [ data, setData ] = useState([])
  const [ loading, setLoading ] = useState(true)
  const [ depositFormData, setDepositFormData ] = useState({})
  const getOrders = useCallback(async () => {
    try {
      setLoading(true)
      const query: any = {

      }
      if(status >= 0) {
        query.status = status
      }
      const result = await Request.get('/order/engineer', query)
      setData(result)
      setLoading(false)
    } catch (e) {
      console.log(e)
    }
  }, [status])

  useEffect(() => {
    getOrders()
  }, [status])

  Taro.useDidShow(() => {
    getOrders()
  })


  return (
    <Container backgroundColor='#F7F8F8'>
      <View className="wapper">
        {
          activeTab === 0 ? (
            <>
              <View className='banner'>
                <Query uri="/banner">
                  {
                    ({ data, loading }) => {
                      if (loading) {
                        return <Loading />
                      }

                      return (
                        <Swiper
                          className='banner__swiper'
                          indicatorColor='#999'
                          indicatorActiveColor='#333'
                          circular
                          indicatorDots
                          autoplay
                        >
                          {
                            data.map(item => (
                              <SwiperItem>
                                <Image mode="aspectFill" src={item.src} />
                              </SwiperItem>
                            ))
                          }
                        </Swiper>
                      )
                    }
                  }
                </Query>
              </View>
              <View className='orderTabs'>
                {
                  tabs.map((item, index) => {
                    return (
                      <View className='orderTabs__item' key={index} onClick={() => {
                        setStatus(item.status)
                      }}
                      >
                        <Image src={item.icon} className='orderTabs__item--img' />
                        <Text className='orderTabs__item--text'>{item.text}</Text>
                      </View>
                    )
                  })
                }
              </View>
              <ScrollView scrollY style={{height: '30%', flexGrow: 1}}>
                {
                  loading ? (
                    <Loading />
                  ) : (
                    data.map(item => {
                      return (
                        <View className='order' onClick={() => {
                          Taro.navigateTo({
                            url: `/pages/engineerOrderDetails/index?id=${item?.id}`
                          })
                        }}>
                          <View className="order__content">
                            <View
                              className='callPhone'
                              onClick={(e) => {
                                e.stopPropagation()
                                Taro.makePhoneCall({
                                  phoneNumber: item.createUser.phoneNumber
                                })
                              }}
                            >
                              <Image src={phone} className='callPhone__img' />
                            </View>
                            <View className='order__preview'>
                              <Image mode="aspectFill" src={item?.images ? item?.images.split(',')[0] : item?.project?.thumbnail} className='order__preview--img' />
                            </View>
                            <View className='order__info'>
                              <Text className='order__info--type'>{item.project?.name}</Text>
                              <Text className='order__info--user'>{item?.createUser?.nickName} {item?.createUser?.phoneNumber}</Text>
                              <Text className='order__info--address'>
                                { item?.address?.province }
                                { item?.address?.city }
                                { item?.address?.area }
                                { item?.address?.address }
                              </Text>
                              <Text className='order__info--address'>
                                { item?.createAt }
                              </Text>
                            </View>
                          </View>
                          <View className='order__actions'>
                            {
                              item?.logicStatus <= 5 && item.upDoorTime && (
                                <Button className='btn'>
                                  { item.upDoorTime ? item.upDoorTime : '设置上门服务时间' }
                                </Button>
                              )
                            }
                            {
                              item.logicStatus === 3 && (
                                <Button
                                  className='btn primary'
                                  onClick={(e) => {
                                    e.stopPropagation()
                                    Taro.showModal({
                                      title: '警告',
                                      content: '请先联系客户约定上门时间，若已经联系过客户，请设置上门时间',
                                      confirmText: '设置时间',
                                      cancelText: '联系客户',
                                      success: (result) => {
                                        if (result.confirm) {
                                          setFormData((v) => ({
                                            ...v,
                                            id: item.id
                                          }))
                                          setIsVisable(true)
                                        } else if (result.cancel) {
                                          Taro.makePhoneCall({
                                            phoneNumber: item.createUser.phoneNumber
                                          })
                                        }
                                      }
                                    })

                                  }}
                                >
                                  预约上门
                                </Button>
                              )
                            }
                            {
                              (item.logicStatus === 4) && (
                                <Button
                                  className='btn primary'
                                  onClick={(e) => {
                                    e.stopPropagation()
                                    Taro.navigateTo({
                                      url: `/pages/quotation/index?id=${item.id}`
                                    })
                                  }}
                                >
                                  收取订金
                                </Button>
                              )
                            }
                            {
                              (item.logicStatus === 6) && (
                                <Mutation uri="/order/complete">
                                  {
                                    (mutation) => (
                                      <Button
                                        className='btn'
                                        onClick={(e) => {
                                          e.stopPropagation()
                                          Taro.navigateTo({
                                            url: `/pages/quotation/index?id=${item.id}`
                                          })
                                        }}
                                      >
                                        修改报价单
                                      </Button>
                                    )
                                  }
                                </Mutation>
                              )
                            }
                            {
                              (item.logicStatus === 6) && (
                                <Mutation uri="/order/complete">
                                  {
                                    (mutation) => (
                                      <Button
                                        className='btn primary'
                                        onClick={(e) => {
                                          e.stopPropagation()
                                          Taro.navigateTo({
                                            url: `/pages/completeOrder/index?id=${item.id}`
                                          })
                                        }}
                                      >
                                        订单完成
                                      </Button>
                                    )
                                  }
                                </Mutation>
                              )
                            }
                          </View>
                        </View>
                      )
                    })
                  )
                }
              </ScrollView>
            </>
          ) : (
            <EngineerMine />
          )
        }
      </View>
      {
        role === 2 && (
          <Tabs list={tabList} onChange={(index) => setActiveTab(index)} />
        )
      }

      <Mutation uri='/order/makeAnAppointment'>
        {
          (mutation, { data, loading }) => {
            return (
              <Dialog
                visable={isVisable}
                title='请选择上门时间'
                onCancel={() => setIsVisable(false)}
                onOk={() => {
                  if(!formData.date || !formData.time) {
                    Taro.showToast({
                      title: '请选择时间',
                      icon: null
                    })
                    return
                  }
                  mutation({
                    ...formData,
                    upDoorTime: `${formData.date} ${formData.time}`
                  })
                    .then(res => {
                      getOrders()
                    })
                  setIsVisable(false)
                }}
              >
                <View className='form'>
                  <View className='form__item'>
                    <Picker
                      onChange={(e) => {
                        setFormData((v) => {
                          return {
                            ...v,
                            date: e.detail.value
                          };
                        })
                      }}
                      mode='date'
                    >
                      <Input className='input' value={formData.date} placeholder='年/月/日' disabled />
                    </Picker>
                  </View>
                  <View className='form__item'>
                    <Picker
                      onChange={(e) => {
                        setFormData((v) => {
                          return {
                            ...v,
                            time: e.detail.value
                          };
                        })
                      }}
                      mode='time'
                    >
                      <Input className='input' value={formData.time} placeholder='时/分' disabled />
                    </Picker>
                  </View>
                </View>
              </Dialog>
            )
          }
        }
      </Mutation>

      <Mutation uri='/order/deposit'>
        {
          (mutation, { data, loading }) => {
            return (
              <Dialog
                visable={isDepositVisable}
                title='请选择您此次需要收取的定金'
                onCancel={() => setIsVisable(false)}
                onOk={() => {
                  console.log(depositFormData)
                  mutation({
                    ...depositFormData,
                  })
                    .then(res => {
                      getOrders()
                      setIsDepositVisable(false)
                    })
                }}
              >
                <View className='form'>
                  <View className='form__item'>
                    <View className='input' onClick={() => setDepositFormData({ ...depositFormData, deposit: 0 })}>
                      无需订金
                    </View>
                  </View>
                  <View className='form__item'>
                    <View className='input' onClick={() => setDepositFormData({ ...depositFormData, deposit: 50 })}>
                      ￥50
                    </View>
                  </View>
                  <View className='form__item'>
                    <Input
                      className='input'
                      placeholder="自定义价格"
                      onInput={(e) => {
                        setDepositFormData({ ...depositFormData, deposit: e.detail.value })
                      }}
                    />
                  </View>
                </View>
              </Dialog>
            )
          }
        }
      </Mutation>

    </Container>
  );
};

export default Workbench;
