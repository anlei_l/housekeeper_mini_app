import {Input, Picker, Switch, Text, View} from "@tarojs/components";
import Taro from "@tarojs/taro";
import Mutation from "../../../components/Mutation";
import {useState} from "react";
import getCurrentPages = Taro.getCurrentPages;

const AddressForm = ({ inititalValues = {}, mutation }) => {
  const [ body, setBody ] = useState(inititalValues)

  return (
    <>
      <View className='address' style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
        <View className='form'>
          <View className='form__title'>
            添加地址
          </View>
          <View className='form__item'>
            <View className='form__item--label'>联系人</View>
            <View className='form__item--content'>
              <Input
                className='input'
                placeholderClass="input__plc"
                value={body.concatName}
                onInput={(e) => {
                  setBody({...body, concatName: e.detail.value})
                }}
                placeholder='请填写联系人姓名'
              />
            </View>
          </View>
          <View className='form__item'>
            <View className='form__item--label'>手机号码</View>
            <View className='form__item--content'>
              <Input
                className='input'
                placeholderClass="input__plc"
                placeholder='请填写手机号码'
                value={body.phoneNo}
                onInput={(e) => {
                  setBody({...body, phoneNo: e.detail.value})
                }}
              />
            </View>
          </View>
          <View className='form__item'>
            <View className='form__item--label'>所在城市</View>
            <View className='form__item--content'>
              <Picker
                mode='region'
                value={['province', 'city', 'area']}
                onChange={(e) => {
                  console.log(e)
                  setBody({
                    ...body,
                    province: e.detail.value[0],
                    city: e.detail.value[1],
                    area: e.detail.value[2]
                  })
                }}
              >
                <Input
                  className='input'
                  placeholderClass="input__plc"
                  placeholder='请选择地址'
                  disabled
                  value={[body.province, body.city, body.area].filter(item => item).join(', ')}
                />
              </Picker>
            </View>
          </View>
          <View className='form__item'>
            <View className='form__item--label'>具体地址</View>
            <View className='form__item--content'>
              <Input
                className='input'
                placeholderClass="input__plc"
                placeholder='请填写街道/小区/办公楼'
                value={body.address}
                onInput={(e) => {
                  setBody({...body, address: e.detail.value})
                }}
              />
            </View>
          </View>
          <View className='form__item orther'>
            <View className='form__item--label'>
              <Text>设置默认地址</Text>
              <Text className='extra'>提示：同城服务默认推荐使用该地址</Text>
            </View>
            <View className='form__item--content'>
              <Switch
                checked={body.isDefault}
                color="#30C3B1"
                className="switch"
                onChange={(e) => {
                  setBody({ ...body, isDefault: e.detail.value })
                }}
              />
            </View>
          </View>
        </View>
      </View>
      <View className='actions'>
        <View
          className='btn'
          onClick={() => {
            mutation(body)
              .then(res => {
                Taro.showToast({
                  title: '操作成功',
                  icon: 'success',
                  duration: 2000
                })
                setTimeout(() => {
                  const pages = getCurrentPages()
                  const current = pages[pages.length - 1]
                  const eventChannel = current.getOpenerEventChannel()
                  eventChannel.emit('refresh')
                  Taro.navigateBack()
                }, 1000)
              })
          }}
        >
          确定
        </View>
      </View>
    </>
  )
}

export default AddressForm
