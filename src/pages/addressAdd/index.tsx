import './index.scss'
import Container from "../../components/Container";
import Mutation from "../../components/Mutation";
import Taro from '@tarojs/taro'
import AddressForm from "./components/AddressForm";


const AddressAdd = () => {
  const { params } = Taro.useRouter()
  return (
    <Container backgroundColor='#F7F8F8'>
      <Mutation uri="/address">
        {
          (mutation, { result, loading }) => (
            <AddressForm inititalValues={{}} mutation={mutation} />
          )
        }
      </Mutation>
    </Container>
  );
};

export default AddressAdd;
