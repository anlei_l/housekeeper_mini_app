import {Button, Input, View} from "@tarojs/components";
import * as React from "react";
import {useEffect, useState} from "react";

const Stepper = ({ inititalValue = 0, onChange }) => {
  const [ value, setValue ] = useState(inititalValue)

  useEffect(() => {
    onChange(value)
  }, [value])

  return (
    <View className='stepper'>
      <View className='stepper__action plus' onClick={() => {value <= 100 && setValue(v => v += 1)}}>+</View>
      <Input className='stepper__input' value={String(value)} onInput={(e) => setValue(Number(e.detail.value))} />
      <View className='stepper__action reduce' onClick={() => { value >= 1 && setValue(v => v -= 1) }}>-</View>
    </View>
  )
}

export default Stepper
