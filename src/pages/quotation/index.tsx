import * as React from "react";
import {Image, View, Text, Button, Input, ScrollView, Picker} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Stepper from "./components/Stepper";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {useCallback, useEffect, useState} from "react";
import Mutation from "../../components/Mutation";
import Loading from "../../components/Loading";
import Request from '../../utils/Request'


const Quotation = () => {
  const router = Taro.useRouter()
  const [ formData, setFormData ] = useState({})
  const [ total, setTotal ] = useState(0)
  const [ data, setData ] = useState<any>({})
  const [ loading, setLoading ] = useState(true)

  useEffect(() => {
    setLoading(true)
    Request.get(`/order/${router.params.id}`)
      .then(res => {
        setData(res)
        res.quotationRecords.map(item => {
          setFormData({
            ...formData,
            [item.quotation.id]: {
              num: item.num,
              price: item.quotation.price * item.num
            }
          })
        })
      })
      .catch((e) => {
        console.log(e)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  useEffect(() => {
    const total = Object.values(formData).reduce((count, item) => (count += item.price),0)
    setTotal(Number(total).toFixed(2))
  }, [formData])

  if (loading) {
    return <Loading />
  }

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <View className="quotation">
        <View className="quotation__info">
          <View className="order">
            <View className="order__title">订单号码</View>
            <View className="order__number">{data?.orderNo}</View>
          </View>
          <View className="actions">
            <Button className="btn">报价单</Button>
          </View>
        </View>
        <Query uri="/category">
          {
            ({ data: category, loading: categoryLoading }) => {
              if (categoryLoading) {
                return <Loading />
              }

              return (
                <View className="product">
                  <View className="product__title">产品信息</View>
                  <ScrollView scrollY className="product__list">
                    {
                      category.map((item, index) => (
                        <View className="product__list--container">
                          <View className="product__name">{ item?.name }</View>
                          <View className="product__list--content">
                            {
                              item.quotations.map(quoataion => (
                                <View className="product__item">
                                  <Text className="product__item--name">{ quoataion.name }</Text>
                                  <Text className="product__item--price">￥{ quoataion.price }</Text>
                                  <Stepper
                                    inititalValue={data.quotationRecords.find(qq => qq.quotation.id === quoataion.id)?.num || 0}
                                    onChange={(num) => {
                                      const quotationCount = {
                                        ...formData,
                                        [quoataion.id]: {
                                          num,
                                          price: quoataion.price * num
                                        }
                                      }
                                      setFormData(quotationCount)
                                    }}
                                  />
                                </View>
                              ))
                            }
                          </View>
                        </View>
                      ))
                    }
                  </ScrollView>
                </View>
              )
            }
          }
        </Query>
      </View>

      <View className="footer">
        <View className="price">
          <Text className="price__title">收取费用</Text>
          <Text className="price__number">￥{ total }</Text>
        </View>
        <Mutation uri="/order/quotation">
          {
            (mutation) => (
              <Button className="action" onClick={() => {
                const quotations = Object.keys(formData).map(item => ({id: item, num: formData[item].num})).filter(item => item.num)
                if (!quotations.length) {
                  Taro.showToast({
                    title: '请选择报价单项'
                  })
                  return
                }
                mutation({
                  id: router.params.id,
                  logicStatus: data.logicStatus,
                  quotation: quotations,
                  total
                })
                  .then(res => {
                    Taro.showToast({
                      title: '提交成功，请等待用户付款',
                      icon: "none"
                    })

                    setTimeout(() => {
                      Taro.redirectTo({
                        url: `/pages/engineerOrderDetails/index?id=${router.params.id}`
                      })
                    }, 2000)
                  })
              }}
              >
                结算
              </Button>
            )
          }
        </Mutation>
      </View>
    </Container>
  );
};

export default Quotation;
