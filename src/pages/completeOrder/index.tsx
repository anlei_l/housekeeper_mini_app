import * as React from "react";
import {Image, View, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {useCallback, useState} from "react";
import Request from '../../utils/Request'
import Mutation from "../../components/Mutation";

import ImagePng from './Image.png'
import Loading from "../../components/Loading";

const productDetails = () => {
  const { params } = Taro.useRouter();
  const [ formData, setFormData ] = useState({
    completePictures: []
  })

  const uploadImage = useCallback(() => {
    Taro.chooseImage({
      count: 1,
      success: (res) => {
        Taro.uploadFile({
          url: `${Request.BASE_URI}/upload`,
          filePath: res.tempFilePaths[0],
          name: 'file',
          formData: {},
          success (res){
            const completePictures = formData.completePictures
            completePictures.push(res.data)
            setFormData({
              ...formData,
              completePictures
            })
            // const data = res.data
            //do something
          }
        })
      }
    })
  }, [formData])

  const save = useCallback((mutation, data) => {
    // if (data.completePictures.length < 3) {
    //   Taro.showToast({
    //     icon: 'none',
    //     title: '请至少上传3张完成图片'
    //   })
    //   return
    // }
    mutation({
      ...data,
      completePictures: formData.completePictures.join(',')
    })
      .then(res => {
        Taro.showToast({
          title: '操作成功',
          icon: 'none'
        })
        setTimeout(() => {
          Taro.redirectTo({
            url: `/pages/engineerOrderDetails/index?id=${res.id}`
          })
        }, 2000)
      })
      .catch(e => {
        console.log(e)
      })
  }, [formData])

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <Query uri={`/order/${params.id}`}>
        {
          ({ data, loading, error }) => {
            if (loading) {
              return <Loading />
            }

            return (
              <>
                <ScrollView scrollY style={{ flexGrow: 1, flexShrink: 1, height: '50%'}}>
                  <View className="content">
                    <View className="problem">
                      <View className="problem__title">上传照片（{formData.completePictures.length}/9）</View>
                      {
                        formData.completePictures.length <= 9 && (
                          <View className="problem__upload" onClick={uploadImage}>
                            <Image src={ImagePng} className="img" />
                          </View>
                        )
                      }
                      {
                        formData.completePictures.map(item => {
                          return (
                            <View className="problem__item">
                              <Image src={item} className="img" />
                            </View>
                          )
                        })
                      }
                    </View>
                  </View>
                </ScrollView>
                <Mutation uri="/order/complete">
                  {
                    (mutation, { loading, error }) => (
                      <View className='actions'>
                        <View />
                        <View className='btn' onClick={() => save(mutation, { ...formData, id: data.id })}>订单完成</View>
                      </View>
                    )
                  }
                </Mutation>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default productDetails;
