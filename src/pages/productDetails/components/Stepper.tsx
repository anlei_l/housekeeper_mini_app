import {Button, Input, View} from "@tarojs/components";
import * as React from "react";
import {useState} from "react";

const Stepper = () => {
  const [ value, setValue ] = useState(1)
  return (
    <View className='stepper'>
      <View className='stepper__action plus' onClick={() => {value <= 100 && setValue(v => v += 1)}}>+</View>
      <Input className='stepper__input' value={String(value)} disabled />
      <View className='stepper__action reduce' onClick={() => { value > 1 && setValue(v => v -= 1) }}>-</View>
    </View>
  )
}

export default Stepper
