import * as React from "react";
import {Image, View, Text, Button, Input, Swiper, SwiperItem, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Taro from '@tarojs/taro'
import Query from "../../components/Query";

import trueIcon from './trueIcon.png'
import message from './message.png'
import market from './market.png'
import timeVerified from './timeVerified.png'
import connection from './connection.png'
import wallet from './wallet.png'
import Loading from "../../components/Loading";

const productDetails = () => {
  const { params } = Taro.useRouter()

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <Query uri={`/project/${params.id}`}>
        {
          ({ data, loading, refresh }) => {
            if (loading) {
              return <Loading/>
            }

            return (
              <>
                <ScrollView scrollY style={{flexGrow: 1, flexShrink: 1, height: '50%'}}>
                  <View className='banner'>
                    <Image className='banner__img' src={data?.banner} />
                  </View>
                  <View className="header">
                    {/*<View className='price'>*/}
                    {/*  <Text className='price__number'>{ data?.price }</Text>*/}
                    {/*  <Text className='price__count'>/次起</Text>*/}
                    {/*</View>*/}
                    <View className='extra'>
                      具体价格工程师上门服务后，清使用计价器核算
                    </View>
                    <View className='characteristic'>
                      <View className='characteristic__item'>
                        <Image src={trueIcon} className="icon" />
                        <Text>保修30天</Text>
                      </View>
                      <View className='characteristic__item'>
                        <Image src={trueIcon} className="icon" />
                        <Text>快速响应</Text>
                      </View>
                      <View className='characteristic__item'>
                        <Image src={trueIcon} className="icon" />
                        <Text>标准价报价</Text>
                      </View>
                    </View>
                  </View>
                  <View className='technological'>
                    <View className='technological__title'>服务流程</View>
                    <View className='technological__process'>
                      <View className='technological__process--item'>
                        <Image src={message} className='icon' />
                        <Text>在线下单</Text>
                      </View>
                      <View className='technological__process--item'>
                        <Image src={market} className='icon' />
                        <Text>检测报价</Text>
                      </View>
                      <View className='technological__process--item'>
                        <Image src={timeVerified} className='icon' />
                        <Text>开始服务</Text>
                      </View>
                      <View className='technological__process--item'>
                        <Image src={connection} className='icon' />
                        <Text>服务验收</Text>
                      </View>
                      <View className='technological__process--item'>
                        <Image src={wallet} className='icon' />
                        <Text>完成服务</Text>
                      </View>
                    </View>
                    <View className='technological__details'>
                      <View className='technological__details--title'>服务详情</View>
                      <View className='technological__details--content'>
                        <View>
                          1. 若由于我们的原因未能完成服务，不收取任何费用。安装维修后同性质且同部位故障保修30天（除自备材料、人为因素、动物破坏、退液、私自拆卸以及其他不可抗力因素导致的故障）
                        </View>
                        <View>
                          2. 我们采用标准报价单进行报价，以保障您的权益，公司严令禁止工程师私下收取额外费用，如有发生，请直接拨打官方客服电话进行投诉。
                        </View>
                        <View>
                          3. 为了保障您的权益和隐私，我们会对您的联系方式进行隐藏，请不要私下与工程师联系服务，感谢支持
                        </View>
                        <View>
                          4. 为了保障您的权益和隐私，我们会对您的联系方式进行隐藏，请不要私下与工程师联系服务，感谢支持
                        </View>
                        <View>
                          5. 服务时段：9：00-20：00
                        </View>
                      </View>
                    </View>
                  </View>
                </ScrollView>
                <View className='actions'>
                  <View className='btn' onClick={() => Taro.navigateTo({url: `/pages/confirmOrder/index?id=${data?.id}`})}>立即下单报修</View>
                </View>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default productDetails;
