import * as React from "react";
import {Image, View, Text, Button, Input, Swiper, SwiperItem, ScrollView, Textarea} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {useCallback, useEffect, useState} from "react";
import Request from '../../utils/Request'
import Mutation from "../../components/Mutation";

import Location from './Location.png'
import arrowRight from './arrowRight.png'
import ImagePng from './Image.png'
import Loading from "../../components/Loading";

const productDetails = () => {
  const { params } = Taro.useRouter();
  const [ formData, setFormData ] = useState({
    images: []
  })

  const uploadImage = useCallback(() => {
    Taro.chooseImage({
      count: 1,
      success: (res) => {
        Taro.uploadFile({
          url: `${Request.BASE_URI}/upload`,
          filePath: res.tempFilePaths[0],
          name: 'file',
          formData: {},
          success (res){
            const images = formData.images
            images.push(res.data)
            setFormData({
              ...formData,
              images
            })
            // const data = res.data
            //do something
          }
        })
      }
    })
  }, [formData])

  const save = useCallback((mutation, data) => {
    if (!data.address) {
      Taro.showToast({
        icon: 'none',
        title: '请选择地址'
      })
      return
    }
    mutation({
      ...data,
      ...formData,
      images: formData.images.join(',')
    })
      .then(res => {
        Taro.showToast({
          title: '下单成功',
          icon: 'none'
        })
        setTimeout(() => {
          Taro.redirectTo({
            url: `/pages/orderDetails/index?id=${res.id}`
          })
        }, 2000)
      })
      .catch(e => {
        console.log(e)
      })
  }, [formData])

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <Query uri={`/project/${params.id}`}>
        {
          ({ data, loading, error }) => {
            if (loading) {
              return <Loading/>
            }

            return (
              <>
                <ScrollView scrollY style={{ flexGrow: 1, flexShrink: 1, height: '50%'}}>
                  <View className="content">
                    <View className='address'>
                      <View className='address__icon'>
                        <Image src={Location} className='address__icon--img' />
                      </View>
                      <View
                        className='address__content'
                        onClick={() => {
                          Taro.navigateTo({
                            url: '/pages/address/index',
                            events: {
                              selectAddress: (address) => {
                                setFormData({
                                  ...formData,
                                  address
                                })
                              }
                            }
                          })
                        }}
                      >
                        {
                          formData?.address
                            ? (
                              <View>
                                { formData?.address?.province } { formData?.address?.city } { formData?.address?.area } { formData?.address?.address }
                                <View>
                                  { formData?.address?.phoneNo } { formData?.address?.concatName }
                                </View>
                              </View>
                            )
                            : (
                              <>
                                <Text>请添加服务地址</Text>
                                <Text>添加</Text>
                              </>
                            )
                        }
                      </View>
                      <Image src={arrowRight} className='address__next' />
                    </View>
                    <View className='project'>
                      <Image src={data?.thumbnail} mode="aspectFill" className='project__preview' />
                      <View className='project__content'>
                        <Text className='project__content--name'>{ data?.category.name }-{data?.name}</Text>
                        <Text className='project__content--info'>工程师上门检测厚，根据标准价目表报价</Text>
                        {/*<Text className='project__content--price'>{ data?.price }元/次起</Text>*/}
                      </View>
                    </View>
                    <View className="remark">
                      <View className="remark__title">备注</View>
                      <View className="remark__content">
                        <Input placeholder="备注" onInput={(evt) => {
                          setFormData({
                            ...formData,
                            remark: evt.detail.value
                          })
                        }} />
                      </View>
                    </View>
                    <View className="problem">
                      <View className="problem__title">上传照片（{formData.images.length}/9）</View>
                      {
                        formData.images.length <= 9 && (
                          <View className="problem__upload" onClick={uploadImage}>
                            <Image src={ImagePng} className="img" />
                          </View>
                        )
                      }
                      {
                        formData.images.map(item => {
                          return (
                            <View className="problem__item">
                              <Image src={item} className="img" />
                            </View>
                          )
                        })
                      }
                    </View>
                  </View>
                </ScrollView>
                <Mutation uri="/order">
                  {
                    (mutation, { loading, error }) => (
                      <View className='actions'>
                        <View className="price">
                          {/*收取费用*/}
                          {/*<Text className="price__number">￥300.00</Text>*/}
                        </View>
                        <View className='btn' onClick={() => save(mutation, { ...formData, project: data })}>下单</View>
                      </View>
                    )
                  }
                </Mutation>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default productDetails;
