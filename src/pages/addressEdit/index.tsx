import './index.scss'
import Container from "../../components/Container";
import Mutation from "../../components/Mutation";
import Taro from '@tarojs/taro'
import Query from "../../components/Query";
import AddressForm from "./components/AddressForm";
import Loading from "../../components/Loading";

const tabs = [{
  text: '全部'
}, {
  text: '待检测'
}, {
  text: '已完成'
}, {
  text: '已取消',
}]

const Order = () => {
  const { params } = Taro.useRouter()
  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri={`/address/${params.id}`}>
        {
          ({ data, loading }) => {
            if (loading) {
              return <Loading/>
            }
            return (
              <Mutation uri="/address" method="put">
                {
                  (mutation, { result, loading }) => (
                    <AddressForm inititalValues={data} mutation={mutation} />
                  )
                }
              </Mutation>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default Order;
