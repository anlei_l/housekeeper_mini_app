import {Image, View, Text, Input, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import DefaultPage from "./components/DefaultPage";
import AddressItem from "./components/AddressItem";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import {useEffect} from "react";
import Loading from "../../components/Loading";

const Address = () => {
  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri="/address">
        {
          ({ data, loading, refresh }) => {
            if (loading) {
              return <Loading/>
            }

            return (
              <>
              <ScrollView scrollY style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
                <View className='addressList'>
                  { !data.length && <DefaultPage /> }
                  {
                    data.map(item => (
                      <AddressItem data={item} onSuccess={refresh} key={item.id} />
                    ))
                  }
                </View>
              </ScrollView>
                <View className="actions">
                  <View className="btn" onClick={(e) => {
                    e.stopPropagation()
                    Taro.navigateTo({
                      url: '/pages/addressAdd/index',
                      events: {
                        'refresh': () => {
                          refresh()
                        }
                      }
                    })
                  }}>新增地址</View>
                </View>
              </>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default Address;
