import * as React from "react";
import {Image, Text, View} from "@tarojs/components";
import '../index.scss'
import Taro from '@tarojs/taro'
import Mutation from "../../../components/Mutation";

import Edit from '../Edit.png'
import Delete from '../Delete.png'

const AddressItem = ({ data, onSuccess }) => {

  const selectAddress = () => {
    const pages = Taro.getCurrentPages()
    const current = pages[pages.length - 1]
    const eventChannel = current.getOpenerEventChannel()
    eventChannel.emit('selectAddress', data)
    Taro.navigateBack()
  }

  return (
    <View className='addressItem' onClick={selectAddress}>
      <View className='addressItem__header'>{data?.city}{data?.area}</View>
      <View className='addressItem__main'>
        {data?.province} {data?.city} {data?.area} {data?.address}
      </View>
      <View className='addressItem__footer'>
        <View className='user'>
          { data?.concatName } { data?.phoneNo }
        </View>
        <View className='btn' onClick={(e) => {
          e.stopPropagation()
          Taro.navigateTo({url: `/pages/addressEdit/index?id=${data?.id}`})
          return
        }}
        >
          <Image src={Edit} className='icon' />
          <Text>修改</Text>
        </View>
        <Mutation uri='/address' method='delete'>
          {
            (mutation) => (
              <View
                className='btn'
                onClick={(e) => {
                  e.stopPropagation()
                  Taro.showModal({
                    title: '警告',
                    content: '您确定要删除这个地址么？',
                    success: function (res) {
                      if (res.confirm) {
                        mutation({id: data?.id})
                          .then(res => {
                            onSuccess && onSuccess()
                          })
                      } else if (res.cancel) {
                        console.log('用户点击取消')
                      }
                    }
                  })
                }}
              >
                <Image src={Delete} className='icon' />
                <Text>删除</Text>
              </View>
            )
          }
        </Mutation>
      </View>
    </View>
  )
}

export default AddressItem
