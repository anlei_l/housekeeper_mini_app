import {Image, View, Text, Input, ScrollView, Switch, Picker} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Taro from '@tarojs/taro'
import Mutation from "../../components/Mutation";
import {useState} from "react";
import Query from "../../components/Query";

const Order = () => {
  const router = Taro.useRouter()
  const [ formData, setFormData ] = useState({
    role: router.params.role
  })

  const [ companyFormData, setCompanyFormData ] = useState()

  //大客户申请
  if(Number(router.params.role) === 3) {
    return (
      <Container backgroundColor='#F7F8F8'>
        <View className='address' style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
          <View className='form'>
            <View className='form__title'>
              大客户信息
            </View>
            <View className='form__item'>
              <View className='form__item--label'>姓名</View>
              <View className='form__item--content'>
                <Input
                  className='input'
                  onInput={(e) => setFormData({...formData, name: e.detail.value})}
                  placeholderClass='input__plc'
                  placeholder='请填写真实姓名'
                />
              </View>
            </View>
            <Mutation uri='/apply-record/big'>
              {
                (mutation) => (
                  <View className='form__item other'>
                    <View className='btn' onClick={() => {
                      mutation(formData)
                        .then(res => {
                          Taro.showToast({
                            title: '申请成功，稍后会有工作人员联系您',
                            icon: null,
                            duration: 2000
                          })
                          setTimeout(() => {
                            Taro.navigateBack()
                          }, 2000)
                        })
                    }}
                    >提交</View>
                  </View>
                )
              }
            </Mutation>
          </View>
        </View>
      </Container>
    )
  }

  //工程师申请
  if(Number(router.params.role) === 2) {
    return (
      <Container backgroundColor='#F7F8F8'>
        <View className='address' style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
          <View className='form'>
            <View className='form__title'>
              工程师信息
            </View>
            <View className='form__item'>
              <View className='form__item--label'>姓名</View>
              <View className='form__item--content'>
                <Input
                  className='input'
                  onInput={(e) => setFormData({...formData, name: e.detail.value})}
                  placeholderClass='input__plc'
                  placeholder='请填写真实姓名'
                />
              </View>
            </View>
            <Query uri='/category'>
              {
                ({ data = [{name: '加载中。。。'}], loading }) => {
                  return (
                    <View className='form__item'>
                      <View className='form__item--label'>技能</View>
                      <View className='form__item--content'>
                        <Picker
                          mode='selector'
                          rangeKey='name'
                          range={[...data]}
                          onChange={
                            (e) => {
                              setFormData({
                                ...formData,
                                skill: data[e.detail.value]
                              })
                            }
                          }
                        >
                          <Input value={formData?.skill?.name} disabled className='input' placeholderClass='input__plc' placeholder='请选择您擅长的技能' />
                        </Picker>
                      </View>
                    </View>
                  )
                }
              }
            </Query>
            {/*<View className='form__item'>*/}
            {/*  <View className='form__item--label'>银行卡号</View>*/}
            {/*  <View className='form__item--content'>*/}
            {/*    <Input*/}
            {/*      className='input'*/}
            {/*      onInput={(e) => setFormData({...formData, bankCardNo: e.detail.value})}*/}
            {/*      placeholderClass='input__plc'*/}
            {/*      placeholder='请填写银行卡号'*/}
            {/*    />*/}
            {/*  </View>*/}
            {/*</View>*/}
            <Mutation uri='/apply-record'>
              {
                (mutation) => (
                  <View className='form__item other'>
                    <View className='btn' onClick={() => {
                      mutation(formData)
                        .then(res => {
                          Taro.showToast({
                            title: '申请成功，稍后会有工作人员联系您',
                            icon: 'none',
                            duration: 2000
                          })
                          setTimeout(() => {
                            Taro.navigateBack()
                          }, 2000)
                        })
                    }}
                    >提交</View>
                  </View>
                )
              }
            </Mutation>
          </View>
        </View>
      </Container>
    )
  }

  //公司申请
  return (
    <Container backgroundColor='#F7F8F8'>
      <View className='address' style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
        <Mutation uri="/company/add">
          {
            (mutation) => (
              <View className='form'>
                <View className='form__title'>
                  公司信息
                </View>
                <View className='form__item'>
                  <View className='form__item--label'>公司名称</View>
                  <View className='form__item--content'>
                    <Input
                      className='input'
                      placeholderClass='input__plc'
                      placeholder='请填写公司名称'
                      onInput={(e) => setCompanyFormData({...companyFormData, name: e.detail.value})}
                    />
                  </View>
                </View>
                <View className='form__item'>
                  <View className='form__item--label'>公司负责人</View>
                  <View className='form__item--content'>
                    <Input
                      className='input'
                      placeholderClass='input__plc'
                      placeholder='请填写公司负责人姓名'
                      onInput={(e) => setCompanyFormData({...companyFormData, administratorName: e.detail.value})}
                    />
                  </View>
                </View>
                <View className='form__item'>
                  <View className='form__item--label'>手机号码</View>
                  <View className='form__item--content'>
                    <Input
                      className='input'
                      placeholderClass='input__plc'
                      placeholder='请填写手机号码'
                      onInput={(e) => setCompanyFormData({...companyFormData, phoneNumber: e.detail.value})}
                    />
                  </View>
                </View>
                <View className='form__item other'>
                  <View className='btn' onClick={() => {
                    mutation(companyFormData)
                      .then(res => {
                        Taro.showToast({
                          title: '申请成功，稍后会有工作人员联系您',
                          icon: null,
                          duration: 2000
                        })
                        setTimeout(() => {
                          Taro.navigateBack()
                        }, 2000)
                      })
                  }}>提交</View>
                </View>
              </View>
            )
          }
        </Mutation>
      </View>
    </Container>
  );
};

export default Order;
