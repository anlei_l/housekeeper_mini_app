import {useCallback, useEffect, useState} from "react";
import { View, Text, Button, Image } from "@tarojs/components";
import { useEnv, useNavigationBar, useModal, useToast } from "taro-hooks";
import Taro from '@tarojs/taro'
import Request from '../../utils/Request'

import './index.scss'
import Loading from "../../components/Loading";
import Container from "../../components/Container";

const Login = () => {
  const [ code, setCode ] = useState('')
  const [ loading, setLoading ] = useState(false)
  const router = Taro.useRouter()

  useEffect(() => {
    setLoading(true)
    Taro.login()
      .then((res) => {
        if (res.code) {
          setCode(res.code)
          setLoading(false)
        }
      })
  }, [])

  const login = useCallback(async (evt) => {
    try{
      const phoneInfo = evt.detail
      const auths = await Taro.getSetting()
      if (!auths?.authSetting['scope.userInfo']) {
        await Taro.authorize({
          scope: 'scope.userInfo'
        })
      }

      const userInfo = await Taro.getUserInfo({
        withCredentials: true,
      })
      const params = {
        code: code,
        invitee: router.params.id,
        userInfo,
        phoneInfo
      }

      const { access_token, ...args } = await Request.post('/user/login', params)
      Taro.setStorageSync({
        key:"access_token",
        data: access_token
      })
      Taro.setStorageSync({
        key:"userInfo",
        data: args
      })
      Taro.redirectTo({
        url: '/pages/index/index'
      })
    } catch (e) {
      console.log(e)
    }
  }, [code])

  if (loading) {
    return <Loading />
  }

  return (
    <Container>
      <View className="actions">
        <Button open-type='getPhoneNumber' onGetPhoneNumber={login} className='btn'>
          使用手机号登录
        </Button>
      </View>
    </Container>
  );
};

export default Login;
