import * as React from "react";
import {Image, View, Text, Button, Input, Swiper, SwiperItem, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import {useEffect, useState} from "react";
import Query from "../../components/Query";
import Taro from '@tarojs/taro'
import Tabs from "../../components/Tabs";
import Mine from "../mine";
import home from './Home.png'
import activeHome from './activeHome.png'
import profile from './profile.png'
import activeprofile from './activeProfile.png'
import Loading from "../../components/Loading";


const tabList = [{
  icon: home,
  activeIcon: activeHome,
  title: '首页',
  url: '/pages/home/index'
}, {
  icon: profile,
  activeIcon: activeprofile,
  title: '我的',
  url: '/pages/mine/index'
}]

const Quotation = () => {
  const [ actived, setActived ] = useState(0)
  const [ activeTab, setActiveTab ] = useState(0)

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      {
        activeTab === 0 ? (
          <>
            <View className='banner'>
              <Query uri="/banner">
                {
                  ({ data, loading }) => {
                    if (loading) {
                      return <Loading />
                    }

                    return (
                      <Swiper
                        className='banner__swiper'
                        indicatorColor='#999'
                        indicatorActiveColor='#333'
                        circular
                        indicatorDots
                        autoplay
                      >
                        {
                          data.map(item => (
                            <SwiperItem>
                              <Image mode="aspectFill" src={item.src} />
                            </SwiperItem>
                          ))
                        }
                      </Swiper>
                    )
                  }
                }
              </Query>
            </View>
            <Query uri="/category">
              {
                ({ data, loading, refresh }) => {
                  if (loading) {
                    return <Loading />
                  }
                  return (
                    <View className='project'>
                      <ScrollView scrollY className='project__category'>
                        {
                          data.sort((a, b) => b.sort - a.sort).map((item, index) => (
                            <View key={index} onClick={() => setActived(index)} className={`project__category--item ${index === actived ? 'actived' : ''}`}>{ item.name }</View>
                          ))
                        }
                      </ScrollView>
                      <ScrollView scrollY className='project__list'>
                        {
                          data[actived] && (
                            <Query uri={`/project`} initialQuery={{category: data[actived].id}}>
                              {
                                ({ data: project = [], loading: projectLoading }) => {
                                  if (projectLoading) {
                                    return <Loading />
                                  }
                                  return (
                                    <View className='project__list--content'>
                                      <View className='project__list--title'>
                                        { data[actived]?.name }
                                      </View>
                                      {
                                        project.map((item, index) => (
                                          <View
                                            className='project__list--item'
                                            key={index}
                                            onClick={
                                              () => Taro.navigateTo({
                                                url: `/pages/productDetails/index?id=${item.id}`
                                              })}
                                          >
                                            <Image mode="aspectFill" src={item?.thumbnail} className='preview' />
                                            <Text className='text'>{ item.name }</Text>
                                          </View>
                                        ))
                                      }
                                    </View>
                                  )
                                }
                              }
                            </Query>
                          )
                        }
                      </ScrollView>
                    </View>
                  )
                }
              }
            </Query>
          </>
        ) : (
          <Mine />
        )
      }

      <Tabs list={tabList} onChange={(index) => setActiveTab(index)} />
    </Container>
  );
};

export default Quotation;
