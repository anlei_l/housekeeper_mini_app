import {Button, Image, Text, View} from "@tarojs/components";
import {useCallback, useEffect, useState} from "react";
import Taro from "@tarojs/taro";
import Container from "../../components/Container";
import user from './user.png'
import tool from './tool.png'
import airplay from './airplay.png'
import home from './home.png'
import './index.scss'
import Request from '../../utils/Request'
import Loading from "../../components/Loading";
import logo from './logo.jpg'

const Index = () => {
  const router = Taro.useRouter()
  const [ code, setCode ] = useState('')
  const [ loading, setLoading ] = useState(false)
  const [ isLogin, setIsLogin ] = useState(false)

  useEffect(() => {
    const access_token = Taro.getStorageSync('access_token')
    setLoading(true)
    if (access_token) {
      Request.get('/user/profile')
        .then(res => {
          Taro.setStorageSync('userInfo', {
            "avatarUrl": res.avatarUrl,
            "id": res.id,
            "nickName": res.nickName,
            "isReceiving": res.isReceiving,
            "role": res.role
          })
          setIsLogin(true)
          setLoading(false)
        })
      return
    }
    Taro.login()
      .then((res) => {
        if (res.code) {
          setCode(res.code)
          setLoading(false)
        }
      })
      .catch(e => {
        console.log(e)
      })
  }, [])

  const login = useCallback(async (evt, path, role) => {
    try{
      if (!isLogin) {
        const phoneInfo = evt.detail
        const auths = await Taro.getSetting()
        if (!auths?.authSetting['scope.userInfo']) {
          await Taro.authorize({
            scope: 'scope.userInfo'
          })
        }

        const userInfo = await Taro.getUserInfo({
          withCredentials: true,
        })
        const params = {
          code: code,
          invitee: router.params.scene,
          userInfo,
          phoneInfo
        }

        const { access_token, ...args } = await Request.post('/user/login', params)
        Taro.setStorageSync('access_token', access_token)
        Taro.setStorageSync('userInfo', args)
      }

      const userInfo = Taro.getStorageSync('userInfo')

      if (!role.includes(userInfo.role)) {
        Taro.showToast({
          icon: 'none',
          title: '对不起您暂无权限'
        })
        setTimeout(() => {
          Taro.redirectTo({
            url: '/pages/index/index'
          })
        }, 800)
        return
      }

      Taro.redirectTo({
        url: path
      })
    } catch (e) {
      console.log(e)
    }
  }, [code, isLogin])

  if (loading) {
    return <Loading />
  }

  if (isLogin) {
    return (
      <Container>
        <View className="bg" key='1111'>
          <Button
            className="entry user"
            onClick={(evt) =>
              login(evt, '/pages/settledForm/index?role=2', [1])}
          >
            <View className="area">
              <Image src={user} />
            </View>
            <Text>工程师入驻</Text>
          </Button>
          <Button
            className="entry tool"
            onClick={(evt) =>
              login(evt, '/pages/home/index', [1])}
          >
            <View className="area">
              <Image src={tool} />
            </View>
            <Text>我要维修</Text>
          </Button>
          <Button
            className="entry airplay"
            onClick={(evt) =>
              login(evt, '/pages/workbench/index', [2, 3])}
          >
            <View className="area">
              <Image src={airplay} />
            </View>
            <Text>工程师登录</Text>
          </Button>
          <Button
            className="entry home"
            onClick={(evt) =>
              login(evt, '/pages/settledForm/index?role=3', [1])}
          >
            <View className="area">
              <Image src={home} />
            </View>
            <Text>大客户入驻</Text>
          </Button>
          <View className="logo">
            <Image src={logo} />
            <Text>房屋管家</Text>
          </View>
        </View>
      </Container>
    )
  }

  return (
    <Container>
      <View className="bg">
        <Button
          open-type='getPhoneNumber'
          className="entry user"
          onGetPhoneNumber={(evt) =>
            login(evt, '/pages/settledForm/index?role=2', [1])}
        >
          <View className="area">
            <Image src={user} />
          </View>
          <Text>工程师入驻</Text>
        </Button>
        <Button
          open-type='getPhoneNumber'
          className="entry tool"
          onGetPhoneNumber={(evt) =>
            login(evt, '/pages/home/index', [1])}
        >
          <View className="area">
            <Image src={tool} />
          </View>
          <Text>我要维修</Text>
        </Button>
        <Button
          open-type='getPhoneNumber'
          className="entry airplay"
          onGetPhoneNumber={(evt) =>
            login(evt, '/pages/workbench/index', [2, 3])}
        >
          <View className="area">
            <Image src={airplay} />
          </View>
          <Text>工程师登录</Text>
        </Button>
        <Button
          open-type='getPhoneNumber'
          className="entry home"
          onGetPhoneNumber={(evt) =>
            login(evt, '/pages/settledForm/index?role=3', [1])}
        >
          <View className="area">
            <Image src={home} />
          </View>
          <Text>大客户入驻</Text>
        </Button>
        <View className="logo">
          <Image src={logo} />
          <Text>房屋管家</Text>
        </View>
      </View>
    </Container>
  )
}

export default Index
