import * as React from "react";
import {Image, View, Text, Button, Input, Swiper, SwiperItem, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Taro from '@tarojs/taro'
import Query from "../../components/Query";

import Loading from "../../components/Loading";

const productDetails = () => {
  const { params } = Taro.useRouter()
  const userInfo = Taro.getStorageSync('userInfo')

  return (
    <Container backgroundColor='#F7F8F8' isTabBar>
      <View className="title">
        <Image mode="aspectFill" src={userInfo?.avatarUrl} />
        {userInfo?.nickName}
      </View>
      <Query uri='/user/singleCode'>
        {
          ({ data, loading, error }) => {
            if (loading) {
              return <Loading />
            }

            return (
              <View className="qrCode">
                <Image src={data?.qrCode} />
                <Text>扫码邀请</Text>
              </View>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default productDetails;
