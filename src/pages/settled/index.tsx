import {Image, View, Text, Input, ScrollView, Switch} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Taro from '@tarojs/taro'

import users from './Users.png'
import officeBuilding from './officeBuilding.png'
import arrowRight from '../workbench/icon/arrowRight.png'


const Settled = () => {
  return (
    <Container backgroundColor='#F7F8F8'>
      <View className='settled' style={{flexShrink: 1, flexGrow: 1, height: '50%'}}>
        <View className='settled__item' onClick={() => {
          Taro.navigateTo({
            url: `/pages/settledForm/index?role=2`
          })
        }}
        >
          <View className='icon'>
            <Image src={users} className='icon__img' />
          </View>
          <Text className='text'>我是工程师</Text>
          <Image src={arrowRight} className='right' />
        </View>
        <View className='settled__item' onClick={() => {
          Taro.navigateTo({
            url: `/pages/settledForm/index?role=3`
          })
        }}
        >
          <View className='icon blue'>
            <Image src={officeBuilding} className='icon__img' />
          </View>
          <Text className='text'>我是公司</Text>
          <Image src={arrowRight} className='right' />
        </View>
      </View>
    </Container>
  );
};

export default Settled;
