import * as React from "react";
import {Image, Text, View} from "@tarojs/components";
import hookpng from '../hook.png'
import phone from '../phone.png'
import moment from "moment";
import Taro from '@tarojs/taro'
import Mutation from "../../../components/Mutation";

const orderStatus = ['已取消', '待检测', '待支付首付款', '进行中', '已完成']

const OrderItem = ({ record, onCancelOrder, onPayDeposit }) => {
  return (
    <View className='orderItem' onClick={() => {
      Taro.navigateTo({
        url: `/pages/orderDetails/index?id=${record.id}`
      })
    }}
    >
      <View className='orderItem__header'>工程师：{record?.engineer?.nickName}</View>
      <View className='orderItem__main'>
        <Image src={record?.project?.thumbnail} mode="aspectFill" className='preview' />
        <View className='info'>
          <View
            className='callPhone'
            onClick={(e) => {
              e.stopPropagation()
              Taro.makePhoneCall({
                phoneNumber: record.engineer.phoneNumber
              })
            }}
          >
            <Image src={phone} className='callPhone__img' />
          </View>
          <View className='info__item'>家电维修-{record?.project?.name}</View>
          <View className='info__item'>{record?.createUser?.nickName} {record?.createUser?.phoneNumber}</View>
          <View className='info__item'>
            {record?.address?.province}
            {record?.address?.city}
            {record?.address?.area}
            {record?.address?.address}
          </View>
          <View className='info__item'>{moment(record?.createAt).format('YYYY-MM-DD HH:mm:ss')}</View>
        </View>
      </View>
      <View className='orderItem__footer'>
        <View className='status'>
          {orderStatus[record?.status]}
        </View>
        {
          record?.status === 4 && (
            <View className='price'>
              收取费用
              <Text className='enhance'>￥{ Number(record?.price - record.deposit).toFixed(2) }</Text>
            </View>
          )
        }
      </View>

      <View className='orderItem__footer ordinary'>
        {
          (record.logicStatus !== 0 && record.logicStatus < 5) &&
          <Mutation uri="/order/cancel">
            {
              (mutation) => (
                <View
                  className='btn'
                  onClick={
                    (e) => {
                      e.stopPropagation()
                      Taro.showModal({
                        title: '提示',
                        content: '您确定要取消这个订单么？',
                        success: (result) => {
                          if (result.confirm) {
                            mutation({ id: record.id })
                              .then(res => {
                                Taro.showToast({
                                  title: '取消成功',
                                  icon: null
                                })
                                onCancelOrder(record.id)
                              })
                          }
                        }
                      })
                    }
                  }
                >
                  取消订单
                </View>
              )
            }
          </Mutation>
        }
        {
          record.logicStatus === 5 && (
            <Mutation uri='/pay/depositInfo'>
              {
                (mutation) => (
                  <View
                    className='btn'
                    onClick={
                      (e) => {
                        e.stopPropagation()

                        mutation(record)
                          .then(res => {
                            Taro.requestPayment({
                              ...res,
                              success: function (res2) {
                                Taro.navigateTo({
                                  url: `/pages/payResult/index?orderNo=${record.orderNo}`
                                })
                              },
                              fail: function (res2) {
                                // Taro.showToast({
                                //   icon: 'none',
                                // })
                              }
                            })
                          })
                        return
                      }
                    }
                  >
                    支付订金
                  </View>
                )
              }
            </Mutation>
          )
        }
        {
          record.logicStatus === 7 && (
            <Mutation uri='/pay/info'>
              {
                (mutation) => (
                  <View
                    className='btn'
                    onClick={
                      (e) => {
                        e.stopPropagation()

                        mutation(record)
                          .then(res => {
                            console.log(res)

                            Taro.requestPayment({
                              ...res,
                              success: function (res2) {
                                Taro.navigateTo({
                                  url: `/pages/payResult/index?orderNo=${record.orderNo}`
                                })
                              }
                            })
                          })

                        return
                      }
                    }
                  >
                    支付费用
                  </View>
                )
              }
            </Mutation>
          )
        }
        <View
          className='btn primary'
          onClick={(e) => {
            e.stopPropagation()
            Taro.makePhoneCall({
              phoneNumber: '18562509436'
            })
          }}
        >
          联系客服
        </View>
      </View>
    </View>
  )
}

export default OrderItem
