import {Image, Text, View} from "@tarojs/components";
import hookpng from '../hook.png'

const DefaultPage = () => {
  return (
    <View className='orderList__null'>
      <Image src={hookpng} className='orderList__null--img' />
      <Text className='orderList__null--text'>暂无订单</Text>
    </View>
  )
}

export default DefaultPage
