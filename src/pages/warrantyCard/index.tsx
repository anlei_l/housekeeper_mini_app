import {Image, View, Text, Input, ScrollView} from "@tarojs/components";
import './index.scss'
import Container from "../../components/Container";
import Search from './Search.png'
import {useState} from "react";
import OrderItem from "./components/OrderItem";
import Query from "../../components/Query";
import Mutation from "../../components/Mutation";
import Loading from "../../components/Loading";
import Taro from '@tarojs/taro'

const tabs = [{
  text: '新订单',
  type: 1
}, {
  text: '待检测',
  type: 2
}, {
  text: '已完成',
  type: 4
}]

const Order = () => {
  const router = Taro.useRouter()
  const [ activeTab, setActiveTab ] = useState(router.params.type)
  return (
    <Container backgroundColor='#F7F8F8'>
      <Query uri='/warranty-card'>
        {
          ({ data, loading, error, refresh }) => {
            return (
              <ScrollView scrollY className="list__container">
                {
                  loading ? (
                    <Loading />
                  ) : (
                    <View className='list'>
                        {
                          data.map(item => (
                            <View className='card' onClick={() => {
                              if (item.order) {
                                Taro.navigateTo({
                                  url: '/pages/orderDetails/index?id=' + item?.order.id
                                })
                              }
                            }}>
                              <View className='card__title'>保修卡：{ item?.cardNo }</View>
                              <View className='card__content'>
                                <View className='project'>{ item?.project?.name }</View>
                                <View className='createAt'>维修时间：{ item?.createAt }</View>
                                <View className='expire'>质保时间：{ item?.expire }</View>
                              </View>
                            </View>
                          ))
                        }
                      </View>
                  )
                }
              </ScrollView>
            )
          }
        }
      </Query>
    </Container>
  );
};

export default Order;
