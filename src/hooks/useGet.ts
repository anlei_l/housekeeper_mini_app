import {useCallback, useEffect, useState} from "react";
import Request from '../utils/Request'

const useGet = (uri, query) => {
  const [ loading, setLoading ] = useState(true)
  const [ result, setResult ] = useState(null)
  const [ error, setError ] = useState('')

  useEffect(() => {
    refresh()
  }, [])

  const refresh = useCallback(async (newQuery = {}) => {
    try {
      setLoading(true)
      const data = await Request.get(uri, {
        ...query,
        ...newQuery
      })
      setResult(data)
      return data
    } catch (e) {
      setError(e)
    } finally {
      setLoading(false)
    }
  }, [])

  return { result, loading, error, refresh }
}

export default useGet
