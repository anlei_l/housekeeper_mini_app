import Taro from '@tarojs/taro'


class Request {
  static BASE_URI = 'https://www.qiaojingling.cn'
  // static BASE_URI = 'http://localhost:3000'

  static request(uri, method, data) {
    return new Promise((resolve, reject) => {
      const access_token = Taro.getStorageSync('access_token')

      Taro.request({
        url: `${Request.BASE_URI}${uri}`,
        data: data,
        method,
        header: {
          'content-type': 'application/json', // 默认值
          'Authorization': `Bearer ${access_token}`
        },
        success: (res) => {
          if (res?.data.statusCode === 401) {
            Taro.setStorageSync('access_token', '')
            Taro.setStorageSync('userInfo', '')
            Taro.navigateTo({
              url: '/pages/index/index'
            })
            return
          }
          if(res?.data?.code === 500) {
            Taro.showToast({
              title: res?.data?.message,
              icon: 'none'
            })
            return new Error(res?.data?.message)
          }
          resolve(res.data)
        },
        fail: (err) => {
          reject(err)
        }
      })
    })
  }
  static get(uri, query) {
    return Request.request(uri, 'get', query)
  }

  static post(uri, data) {
    return Request.request(uri, 'post', data)
  }

  static put(uri, data) {
    return Request.request(uri, 'put', data)
  }

  static delete(uri, data) {
    return Request.request(`${uri}/${data.id}`, 'delete')
  }
}

export default Request
