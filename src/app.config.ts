export default {
  pages: [
    "pages/index/index", //首页
    "pages/home/index", //首页
    "pages/login/index", //登陆页
    "pages/address/index", //地址管理
    "pages/settledForm/index", //入驻表单
    "pages/settled/index", //入驻
    "pages/addressEdit/index", //地址修改
    "pages/addressAdd/index", //地址修改
    "pages/order/index", //工程师订单
    "pages/orderDetails/index", // 工程师订单详情
    "pages/confirmOrder/index", //下单
    "pages/productDetails/index", //维修项目详情
    "pages/workbench/index", //工程师 工作台
    "pages/quotation/index", //工程师报价单
    "pages/payResult/index", //工程师报价单
    "pages/singleCode/index", //下单码
    "pages/shareRecord/index", //分成记录
    "pages/warrantyCard/index", //保修卡
    "pages/engineerOrderDetails/index", //工程师订单详情
    "pages/completeOrder/index", //订单完成
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },

  usingComponents: {}
};
